//
//  NewsPreview.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

public struct NewsPreview: Codable, Equatable {
    public var image: ImageDescriptor
    public var date: Date
    public var title: String
}
