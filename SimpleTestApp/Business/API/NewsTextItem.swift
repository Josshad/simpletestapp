//
//  NewsTextItem.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

public struct NewsTextItem: Codable, Equatable {
    public enum Style: String, Codable {
        case title = "title"
        case subtitle = "subtitle"
        case lead = "lead"
        case text = "text"
    }
    public var type: Style
    public var content: String
}
