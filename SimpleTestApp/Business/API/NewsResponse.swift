//
//  NewsResponse.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

public struct NewsResponse: Codable, Equatable {
    public var id: String
    public var header: String
    public var items: [NewsItem]
}
