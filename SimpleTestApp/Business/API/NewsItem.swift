//
//  NewsItem.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

public struct NewsItem: Codable, Equatable {
    public var contentItem: NewsContentItem
    public var preview: NewsPreview
}
