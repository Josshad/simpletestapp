//
//  ImageDescriptor.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

public struct ImageDescriptor: Codable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case singleResolutionURL = "1x"
        case doubleResolutionURL = "2x"
        case tripleResolutionURL = "3x"
        case aspectRatio = "aspectRatio"
        case loopAnimation = "loopAnimation"
    }
    public var singleResolutionURL: URL
    public var doubleResolutionURL: URL
    public var tripleResolutionURL: URL
    public var aspectRatio: Float
    public var loopAnimation: Bool?
}
