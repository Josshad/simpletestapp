//
//  NewsDetails.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

public struct NewsDetails: Codable, Equatable {
    public var backgroundImage: ImageDescriptor
    public var items: [NewsTextItem]
}
