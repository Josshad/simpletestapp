//
//  NewsAdapter.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

protocol NewsAdapter {
    var news: Observable<[NewsItem]> { get }
    
    func refresh() -> Completable
    func loadDetails(_ url: URL) -> Single<NewsDetails>
}

struct NewsAdapterImpl: NewsAdapter {
    private let service: NewsService = NewsServiceImpl(httpClient: HTTPClient.shared)
    
    let news: Observable<[NewsItem]>
    private let fetchNews: Single<[NewsItem]>
    private let reload = PublishRelay<Void>()
    
    init() {
        fetchNews = Observable
            .deferred { [service] in service.fetchNews().asObservable() }
            .map { $0.items }
            .share()
            .asSingle()
        
        news = reload.asObservable()
            .flatMapLatest { [fetchNews] _ in fetchNews.asObservable().ignoreError() }
            .share(replay: 1, scope: .forever)
    }
    
    func refresh() -> Completable {
        fetchNews
            .do(onSubscribe: reload.accept)
            .asCompletable()
    }
    
    func loadDetails(_ url: URL) -> Single<NewsDetails> {
        service
            .fetchNewsContent(url)
    }
}
