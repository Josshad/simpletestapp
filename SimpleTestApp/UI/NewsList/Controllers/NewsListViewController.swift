//
//  NewsListViewController.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class NewsListViewController: UIViewController {
    
    private let disposeBag = DisposeBag()
    private let viewModel: NewsListViewModel
    init(viewModel: NewsListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    private lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.tintColor = .gray
        control.addTarget(self, action: #selector(refreshContent), for: .valueChanged)
        return control
    }()
    
    private lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout.vertical
        flowLayout.sectionHeadersPinToVisibleBounds = true
        let cv = UICollectionView(frame: .zero,
                                  collectionViewLayout: flowLayout)
        
        cv.backgroundColor = .white
        cv.showsVerticalScrollIndicator = false
        cv.contentInsetAdjustmentBehavior = .automatic
        cv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cv.alwaysBounceVertical = true
        cv.refreshControl = refreshControl
        cv.register(NewsCell.self)
        cv.register(NewsHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
        return cv
    }()
    
    private lazy var dataSource = RxCollectionViewSectionedReloadDataSource<NewsListSectionModel>(configureCell: { _, cv, indexPath, model in
        let cell = cv.dequeueReusableCell(NewsCell.self, for: indexPath)
        cell.title = model.preview.title
        cell.subtitle = RelativeDateTimeFormatter.st_default.localizedString(for: model.preview.date, relativeTo: Date())
        cell.loadImageForURL(model.preview.image.url)
        return cell
    }, configureSupplementaryView: { (ds, cv, kind, indexPath) -> UICollectionReusableView in
        let model = ds[indexPath.section].model
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let header = cv.dequeueReusableSupplementaryView(NewsHeader.self,
                                                             ofKind: UICollectionView.elementKindSectionHeader,
                                                             for: indexPath)
            header.title = model
            return header
        default:
            return UICollectionReusableView()
        }
    })
    
    override func loadView() {
        let view = UIView(with: .white)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(collectionView)
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        bindUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.safeAreaLayoutGuide.layoutFrame
    }
    
    private func bindUI() {
        viewModel.items
            .distinctUntilChanged()
            .drive(collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        collectionView
            .rx.itemSelected
            .bind(to: viewModel.selectIndexPath)
            .disposed(by: disposeBag)
        
        viewModel.refresh()
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    @objc
    private func refreshContent() {
        viewModel.refresh()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onCompleted: { [refreshControl] in
                refreshControl.endRefreshing()
            }, onError: { [refreshControl] _ in
                refreshControl.endRefreshing()
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension NewsListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ cv: UICollectionView,
        layout _: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: NewsCell.preferredHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: NewsHeader.preferredHeight)
    }
}
