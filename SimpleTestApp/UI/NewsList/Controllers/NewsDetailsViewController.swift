//
//  NewsDetailsViewController.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Kingfisher

class NewsDetailsViewController: UIViewController {
    private enum Const {
        static let intercellSpacing: CGFloat = 16
    }

    private let disposeBag = DisposeBag()
    private let viewModel: NewsDetailsViewModel
    init(viewModel: NewsDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .fullScreen
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    private lazy var detailsView: NewsDetailsView = {
        let view = NewsDetailsView(with: collectionView)
        view.backgroundColor = .white
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return view
    }()
    
    lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero,
                                  collectionViewLayout: UICollectionViewFlowLayout.vertical)
        
        cv.backgroundColor = .clear
        cv.showsVerticalScrollIndicator = false
        cv.contentInsetAdjustmentBehavior = .automatic
        cv.alwaysBounceVertical = true
        cv.contentInset = UIEdgeInsets(top: NewsDetailsView.preferredImageHeight, left: 0, bottom: 0, right: 0)
        cv.register(NewsDetailsTitleCell.self)
        cv.register(NewsDetailsLeadCell.self)
        cv.register(NewsDetailsTextCell.self)
        cv.register(NewsDetailsHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
        return cv
    }()
    
    private lazy var dataSource = RxCollectionViewSectionedReloadDataSource<NewsDetailsSectionModel>(configureCell: { _, cv, indexPath, model in
        let cell: UICollectionViewCell
        switch model.type {
        case .title, .subtitle:
            cell = cv.dequeueReusableCell(NewsDetailsTitleCell.self, for: indexPath)
        case .lead:
            cell = cv.dequeueReusableCell(NewsDetailsLeadCell.self, for: indexPath)
        case .text:
            cell = cv.dequeueReusableCell(NewsDetailsTextCell.self, for: indexPath)
        }
        if let newsCell = cell as? NewsDetailsCell {
            newsCell.attributedText = model.attributedText
        }
        return cell
    }, configureSupplementaryView: { (ds, cv, kind, indexPath) -> UICollectionReusableView in
        let model = ds[indexPath.section].model
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            return cv.dequeueReusableSupplementaryView(NewsDetailsHeader.self,
                                                       ofKind: UICollectionView.elementKindSectionHeader,
                                                       for: indexPath)
        default:
            return UICollectionReusableView()
        }
    })
    
    override func loadView() {
        view = detailsView
    }
    
    override func viewDidLoad() {
        super .viewDidLoad()
        collectionView.delegate = self
        bindUI()
    }
    
    private func bindUI() {
        viewModel.backgroundImage
            .compactMap({ $0 })
            .drive(detailsView.rx.imageDescriptor)
            .disposed(by: disposeBag)
        
        viewModel.items
            .distinctUntilChanged()
            .drive(collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        detailsView.rx.tapClose
            .bind(to: viewModel.close)
            .disposed(by: disposeBag)
        
        collectionView
            .rx.contentOffset
            .map({ -$0.y })
            .bind(to: detailsView.rx.imageHeight)
            .disposed(by: disposeBag)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension NewsDetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ cv: UICollectionView,
        layout _: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        guard let items = dataSource.sectionModels.last?.items,
            indexPath.item < items.count else {
                return .zero
        }
        let item = items[indexPath.item]
        let width = collectionView.bounds.width
        let height: CGFloat
        switch item.type {
        case .title, .subtitle:
            height = NewsDetailsTitleCell.preferredHeight(for: item.attributedText, width: width)
        case .lead:
            height = NewsDetailsLeadCell.preferredHeight(for: item.attributedText, width: width)
        case .text:
            height = NewsDetailsTextCell.preferredHeight(for: item.attributedText, width: width)
        }
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        Const.intercellSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: NewsDetailsHeader.preferredHeight)
    }
}
