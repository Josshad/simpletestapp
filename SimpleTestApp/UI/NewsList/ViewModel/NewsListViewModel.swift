//
//  NewsListViewModel.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

typealias NewsListSectionModel = SectionModel<String, NewsItem>

protocol NewsListViewModel {
    // out
    var items: Driver<[NewsListSectionModel]> { get }
    
    // in
    var selectIndexPath: PublishRelay<IndexPath> { get }
    func refresh() -> Completable
}

enum NewsListNavigation {
    case openDetails(URL)
}

protocol NewsListNavigationProvider {
    var navigation: Signal<NewsListNavigation> { get }
}

struct NewsListViewModelImpl: NewsListViewModel, NewsListNavigationProvider {
    let items: Driver<[NewsListSectionModel]>
    let navigation: Signal<NewsListNavigation>
    let selectIndexPath: PublishRelay<IndexPath> = PublishRelay()
    
    private enum Const {
        static let sectionTitle = "News"
        static let emptySection = NewsListSectionModel(model: Const.sectionTitle, items: [])
    }
    
    private let disposeBag = DisposeBag()
    private let adapter: NewsAdapter
    private let itemsRelay: BehaviorRelay<[NewsListSectionModel]> = BehaviorRelay(value: [Const.emptySection])
    private let navigationRelay: PublishRelay<NewsListNavigation> = PublishRelay()
    
    init(adapter: NewsAdapter) {
        self.adapter = adapter
        items = itemsRelay.distinctUntilChanged().asDriver(onErrorJustReturn: [Const.emptySection])
        navigation = navigationRelay.asSignal()
        
        adapter.news
            .map({ [NewsListSectionModel(model: Const.sectionTitle, items: $0)] })
            .bind(to: itemsRelay)
            .disposed(by: disposeBag)
        
        selectIndexPath
            .withLatestFrom(itemsRelay) { ip, section -> URL? in
                guard let items = section.last?.items,
                    ip.item < items.count else {
                    return nil
                }
                return items[ip.item].contentItem.url
            }
            .compactMap({ $0 })
            .map({ NewsListNavigation.openDetails($0) })
            .bind(to: navigationRelay)
            .disposed(by: disposeBag)
    }
    
    func refresh() -> Completable {
        adapter.refresh()
    }
}
