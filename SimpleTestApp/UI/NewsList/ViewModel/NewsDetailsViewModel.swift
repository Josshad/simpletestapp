//
//  NewsDetailsViewModel.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources
import MarkdownKit

typealias NewsDetailsSectionModel = SectionModel<String, NewsTextItem>

protocol NewsDetailsViewModel {
    // out
    var items: Driver<[NewsDetailsSectionModel]> { get }
    var backgroundImage: Driver<ImageDescriptor?> { get }
    
    // in
    var close: PublishRelay<Void> { get }
}

enum NewsDetailsNavigation {
    case close
}

protocol NewsDetailsNavigationProvider {
    var navigation: Signal<NewsDetailsNavigation> { get }
}

struct NewsDetailsViewModelImpl: NewsDetailsViewModel, NewsDetailsNavigationProvider {
    let items: Driver<[NewsDetailsSectionModel]>
    let backgroundImage: Driver<ImageDescriptor?>
    let navigation: Signal<NewsDetailsNavigation>
    
    let close: PublishRelay<Void> = PublishRelay()
    
    private let disposeBag = DisposeBag()
    private let itemsRelay: BehaviorRelay<[NewsDetailsSectionModel]> = BehaviorRelay(value: [])
    private let backgroundImageRelay: BehaviorRelay<ImageDescriptor?> = BehaviorRelay(value: nil)
    init(adapter: NewsAdapter,
         url: URL) {
        items = itemsRelay.asDriver()
        backgroundImage = backgroundImageRelay.asDriver()
        navigation = close.asSignal().map({ NewsDetailsNavigation.close })
        
        let details = adapter.loadDetails(url)
            .asObservable()
            .catchError({ e in
                print("Error on load details: \(e.localizedDescription)")
                return .never()
            })
            .share()
        
        details
            .map({ [NewsDetailsSectionModel(model: "", items: $0.items)] })
            .bind(to: itemsRelay)
            .disposed(by: disposeBag)
        
        details
            .map({ $0.backgroundImage })
            .bind(to: backgroundImageRelay)
            .disposed(by: disposeBag)
    }
}

extension NewsTextItem {
    var attributedText: NSAttributedString {
        switch type {
        case .title:
            return NSAttributedString(string: content, attributes: [.font: UIFont.mainHeading])
        case .subtitle:
            return NSAttributedString(string: content, attributes: [.font: UIFont.title])
        default:
            return MarkdownParser.st_default.parse(content)
        }
        
    }
}
