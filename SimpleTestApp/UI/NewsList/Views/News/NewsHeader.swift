//
//  NewsFooter.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class NewsHeader: UICollectionReusableView {
    
    private enum Metrics {
        static let sideInset: CGFloat = 24
        static let height: CGFloat = 48
    }
    
    static let preferredHeight = Metrics.height
    
    fileprivate let titleLabel: UILabel = .mainHeadingBlackLeft
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        addSubview(titleLabel)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.frame = bounds.insetBy(dx: Metrics.sideInset, dy: 0)
    }
}

extension NewsHeader {
    var title: String? {
        get {
            titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
}
