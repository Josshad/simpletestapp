//
//  NewsCell.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit
import Kingfisher

class NewsCell: UICollectionViewCell {
    
    private enum Metrics {
        static let iconSide: CGFloat = 40
        static let iconCornerRadius: CGFloat = 8
        static let textToIconSpace: CGFloat = 16
        
        static let contentInsets = UIEdgeInsets(top: 12, left: 24, bottom: 12, right: 24)
        static let titleHeight: CGFloat = 40
        static let subtitleHeight: CGFloat = 16
        static let interLabelsSpace: CGFloat = 0
        static let separatorHeight: CGFloat = 1
        
        static let cellHeight: CGFloat = 80
    }
    
    static let preferredHeight: CGFloat = Metrics.cellHeight
    
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        view.layer.cornerRadius = Metrics.iconCornerRadius
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel.titleBlackLeft
        label.numberOfLines = 2
        return label
    }()
    
    fileprivate let subtitleLabel: UILabel = .captionGreyLeft
    private let separator = UIView(with: .st_lightGrey)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = .white
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(iconView)
        contentView.addSubview(separator)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconView.kf.cancelDownloadTask()
        iconView.image = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconView.frame = CGRect(x: Metrics.contentInsets.left,
                                y: Metrics.contentInsets.top,
                                width: Metrics.iconSide,
                                height: Metrics.iconSide)
        let labelsX = iconView.frame.maxX + Metrics.textToIconSpace
        let labelsWidth = bounds.width - labelsX - Metrics.contentInsets.right
        titleLabel.frame = CGRect(x: labelsX,
                                  y: Metrics.contentInsets.top,
                                  width: labelsWidth,
                                  height: Metrics.titleHeight)
        subtitleLabel.frame = CGRect(x: labelsX,
                                     y: titleLabel.frame.maxY + Metrics.interLabelsSpace,
                                     width: labelsWidth,
                                     height: Metrics.subtitleHeight)
        separator.frame = CGRect(x: labelsX,
                                 y: bounds.maxY - Metrics.separatorHeight,
                                 width: bounds.width - labelsX,
                                 height: Metrics.separatorHeight)
    }
}

extension NewsCell {
    var title: String? {
        get {
            titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    var subtitle: String? {
        get {
            subtitleLabel.text
        }
        set {
            subtitleLabel.text = newValue
        }
    }
    
    func loadImageForURL(_ url: URL) {
        iconView.kf.setImage(with: url)
    }
}
