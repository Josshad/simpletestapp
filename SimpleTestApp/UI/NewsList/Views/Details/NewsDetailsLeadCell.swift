//
//  NewsDetailsLeadCell.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class NewsDetailsLeadCell: UICollectionViewCell, PreferredHeightView {
    
    private enum Metrics {
        static let sideInset: CGFloat = 24
        static let leadWidth: CGFloat = 2
        static let leadToTextSpace: CGFloat = 14
    }
    
    fileprivate lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var leadLine: UIView = {
        let view = UIView(with: .darkGray)
        view.layer.cornerRadius = Metrics.leadWidth/2
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = .white
        contentView.addSubview(textLabel)
        contentView.addSubview(leadLine)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let width = bounds.width - Metrics.sideInset * 2 - Metrics.leadWidth - Metrics.leadToTextSpace
        let height = NewsDetailsLeadCell.textHeight(for: textLabel.attributedText, width: width)
        leadLine.frame = CGRect(x: Metrics.sideInset,
                                y: 0,
                                width: Metrics.leadWidth,
                                height: height)
        textLabel.frame = CGRect(x: Metrics.sideInset + Metrics.leadWidth + Metrics.leadToTextSpace,
                                 y: 0,
                                 width: width,
                                 height: height)
        
    }
    
    private static func textHeight(for text: NSAttributedString?, width: CGFloat) -> CGFloat {
        guard let text = text else { return 0 }
        return text.height(forWidth: width)
    }
    
    static func preferredHeight(for text: NSAttributedString, width: CGFloat) -> CGFloat {
        textHeight(for: text,
                   width: width - Metrics.sideInset * 2 - Metrics.leadWidth - Metrics.leadToTextSpace)
    }
}

extension NewsDetailsLeadCell: NewsDetailsCell {
    var attributedText: NSAttributedString? {
        get {
            textLabel.attributedText
        }
        set {
            textLabel.attributedText = newValue
        }
    }
}
