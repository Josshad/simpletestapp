//
//  NewsDetailsView.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NewsDetailsView: UIView {
    private enum Metrics {
        static let buttonSide: CGFloat = 24
        static let buttonSideInset: CGFloat = 24
        static let imageHeight: CGFloat = UIScreen.main.bounds.height / 3
        static let imageHeightAddition: CGFloat = 12
    }
    
    static let preferredImageHeight = Metrics.imageHeight
    
    private let collectionView: UICollectionView
    fileprivate lazy var backgroundImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .top
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(.commonClose, for: .normal)
        button.tintColor = .white
        button.backgroundColor = .gray
        button.layer.cornerRadius = Metrics.buttonSide/2
        return button
    }()
    
    fileprivate var imageHeight: CGFloat = Metrics.imageHeight {
        didSet {
            if imageHeight != oldValue {
                updateImageRatio()
                setNeedsLayout()
            }
        }
    }
    
    private var correctImageHeight: CGFloat {
        imageHeight + Metrics.imageHeightAddition
    }
    
    override var frame: CGRect {
        didSet {
            updateImageRatio()
        }
    }
    
    init(with collectionView: UICollectionView) {
        self.collectionView = collectionView
        super.init(frame: .zero)
        
        addSubview(backgroundImage)
        addSubview(collectionView)
        addSubview(closeButton)
        self.clipsToBounds = true
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundImage.frame = CGRect(x: 0, y: 0, width: bounds.width, height: correctImageHeight)
        collectionView.frame = bounds
        closeButton.frame = CGRect(x: bounds.width - Metrics.buttonSide - Metrics.buttonSideInset,
                                   y: safeAreaInsets.top + Metrics.buttonSideInset,
                                   width: Metrics.buttonSide,
                                   height: Metrics.buttonSide)
    }
    
    fileprivate func updateImageRatio() {
        let width = bounds.width
        guard width > 0,
            let image = backgroundImage.image else { return }
        let ratio: CGFloat = max(width/image.size.width, correctImageHeight/image.size.height)
        backgroundImage.transform = CGAffineTransform(scaleX: ratio, y: ratio)
    }
}

extension Reactive where Base: NewsDetailsView {
    var imageDescriptor: Binder<ImageDescriptor> {
        Binder(base) { view, imageDescriptor in
            let view = view as NewsDetailsView
            view.backgroundImage.kf.setImage(with: imageDescriptor.url) { [view] result in
                switch result {
                case .success:
                    view.updateImageRatio()
                default:
                    return
                }
            }
        }
    }
    
    var tapClose: ControlEvent<Void> {
        base.closeButton
            .rx.tap
    }
    
    var imageHeight: Binder<CGFloat> {
        Binder(base) { view, imageHeight in
            view.imageHeight = imageHeight
        }
    }
}
