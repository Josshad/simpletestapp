//
//  NewsDetailsHeader.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class NewsDetailsHeader: UICollectionReusableView {
    private enum Metrics {
        static let cornerRadius: CGFloat = 12
        static let height: CGFloat = 24
    }
    
    static let preferredHeight = Metrics.height
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = Metrics.cornerRadius
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
}
