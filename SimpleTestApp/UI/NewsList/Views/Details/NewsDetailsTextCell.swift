//
//  NewsDetailsTextCell.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

class NewsDetailsTextCell: UICollectionViewCell, PreferredHeightView {
    
    private enum Metrics {
        static let sideInset: CGFloat = 24
    }
    
    fileprivate lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = .white
        contentView.addSubview(textLabel)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unavailable")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let width = bounds.width - Metrics.sideInset * 2
        let height = NewsDetailsTextCell.textHeight(for: textLabel.attributedText, width: width)
        textLabel.frame = CGRect(x: Metrics.sideInset,
                                  y: 0,
                                  width: width,
                                  height: height)
        
    }
    
    private static func textHeight(for text: NSAttributedString?, width: CGFloat) -> CGFloat {
        guard let text = text else { return 0 }
        return text.height(forWidth: width)
    }
    
    static func preferredHeight(for text: NSAttributedString, width: CGFloat) -> CGFloat {
        textHeight(for: text, width: width - Metrics.sideInset * 2)
    }
}

extension NewsDetailsTextCell: NewsDetailsCell {
    var attributedText: NSAttributedString? {
        get {
            textLabel.attributedText
        }
        set {
            textLabel.attributedText = newValue
        }
    }
}
