//
//  FlowController.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit
import RxSwift

class NewsFlowController {
    private let disposeBag = DisposeBag()
    private let adapter: NewsAdapter
    
    lazy var rootViewController: UIViewController = {
        let controller = UINavigationController(rootViewController: newsController)
        controller.navigationBar.isHidden = true
        return controller
    }()
    
    init(adapter: NewsAdapter = NewsAdapterImpl()) {
        self.adapter = adapter
    }
    
    private lazy var newsController: UIViewController = {
        let viewModel = NewsListViewModelImpl(adapter: adapter)
        viewModel.navigation
            .emit(onNext: { [weak self] in
                guard let self = self else { return }
                switch $0 {
                case .openDetails(let url):
                    let controller = self.instantiateDetailsController(detailsURL: url)
                    self.show(controller)
                }
            })
            .disposed(by: disposeBag)
        return NewsListViewController(viewModel: viewModel)
    }()
    
    private func instantiateDetailsController(detailsURL: URL) -> UIViewController {
        let viewModel = NewsDetailsViewModelImpl(adapter: adapter, url: detailsURL)
        viewModel.navigation
            .emit(onNext: { [weak self] in
                guard let self = self else { return }
                switch $0 {
                case .close:
                    self.dismiss()
                }
            })
            .disposed(by: disposeBag)
        return NewsDetailsViewController(viewModel: viewModel)
    }
    
    private func show(_ controller: UIViewController) {
        self.rootViewController.present(controller, animated: true, completion: nil)
    }
    
    private func dismiss() {
        self.rootViewController.dismiss(animated: true, completion: nil)
    }
}
