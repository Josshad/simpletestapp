//
//  Reusable.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

public protocol ReusableView: AnyObject {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    public static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

extension UICollectionReusableView: ReusableView { }

extension UICollectionView {
    
    func register<T: UICollectionViewCell>(_: T.Type) {
        register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
}

extension UICollectionView {
    
    func register<T: UICollectionReusableView>(_: T.Type, forSupplementaryViewOfKind kind: String) {
        register(T.self, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.defaultReuseIdentifier)
    }
    
}

extension UICollectionView {
    
    func dequeueReusableCell<T>(_: T.Type, withReuseIdentifier identifier: String, for indexPath: IndexPath) -> T {
        let cell = dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        guard let typedCell = cell as? T else {
            fatalError("Incorrect type of cell")
        }
        
        return typedCell
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(_ type: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableCell(type, withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath)
    }
}

extension UICollectionView {
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(_: T.Type,
                                                                       ofKind kind: String,
                                                                       withReuseIdentifier reuseIdentifier: String,
                                                                       for indexPath: IndexPath) -> T {
        let view = dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: reuseIdentifier, for: indexPath)
        guard let typedView = view as? T else {
            fatalError("Incorrect type of supplementary view")
        }
        
        return typedView
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(_ type: T.Type,
                                                                       ofKind kind: String,
                                                                       for indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(type,
                                                ofKind: kind,
                                                withReuseIdentifier: T.defaultReuseIdentifier,
                                                for: indexPath)
    }
    
}
