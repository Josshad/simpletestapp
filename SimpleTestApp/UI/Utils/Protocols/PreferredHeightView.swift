//
//  PreferredSizeView.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

protocol PreferredHeightView {
    static func preferredHeight(for text: NSAttributedString, width: CGFloat) -> CGFloat
}
