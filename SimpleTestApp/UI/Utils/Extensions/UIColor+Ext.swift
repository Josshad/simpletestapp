//
//  UIColor+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

// MARK: Hex
extension UIColor {
    private enum Const {
        static let componentMultiplier: CGFloat = 255
    }
    
    var st_hexColor: Int {
        var rComponent: CGFloat = 0
        var gComponent: CGFloat = 0
        var bComponent: CGFloat = 0
        self.getRed(&rComponent, green: &gComponent, blue: &bComponent, alpha: nil)
        
        return Int(ceilf(Float(rComponent * Const.componentMultiplier)) * 0xFF * 0xFF +
            ceil(Float(gComponent * Const.componentMultiplier)) * 0xFF +
            ceilf(Float(bComponent * Const.componentMultiplier)))
    }
    
    static func color(with hexColor:Int) -> UIColor {
        let bComponent = CGFloat(hexColor & 0xFF)
        let gComponent = CGFloat(hexColor >> 8 & 0xFF)
        let rComponent = CGFloat(hexColor >> 16 & 0xFF)
        
        return UIColor(red: rComponent/Const.componentMultiplier,
                       green: gComponent/Const.componentMultiplier,
                       blue: bComponent/Const.componentMultiplier,
                       alpha: 1)
    }
}

// MARK: Default colors
extension UIColor {
    static let st_lightGrey: UIColor = .color(with: 0xDFDFDF)
}
