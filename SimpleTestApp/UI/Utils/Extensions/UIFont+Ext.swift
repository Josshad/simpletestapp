//
//  UIFont+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

extension UIFont {
    static let mainHeading: UIFont = .systemFont(ofSize: 24, weight: .heavy)
    
    static let title: UIFont = .systemFont(ofSize: 16, weight: .bold)
    
    static let body: UIFont = .systemFont(ofSize: 16, weight: .regular)
    static let bodyItalic: UIFont = .italicSystemFont(ofSize: 16)
    
    static let caption: UIFont = .systemFont(ofSize: 12, weight: .regular)
}
