//
//  UIView+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

extension UIView {
    convenience init(with backgroundColor: UIColor) {
        self.init()
        self.backgroundColor = backgroundColor
    }
}
