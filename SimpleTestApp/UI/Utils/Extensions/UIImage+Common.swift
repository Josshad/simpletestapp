//
//  UIImage+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

extension UIImage {
    private enum Name {
        static let close = "Common.Close"
    }
    
    static var commonClose: UIImage {
        guard let image = UIImage(named: Name.close) else {
            fatalError("Can't find image for commonClose icon")
        }
        return image
    }
}
