//
//  UILabel+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

extension UILabel {
    static var mainHeadingBlackLeft: UILabel {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.font = .mainHeading
        return label
    }
    
    static var titleBlackLeft: UILabel {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.font = .title
        return label
    }
    
    static var bodyBlackLeft: UILabel {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.font = .body
        return label
    }
    
    static var bodyItalicGreyLeft: UILabel {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .left
        label.font = .bodyItalic
        return label
    }
    
    static var captionGreyLeft: UILabel {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .left
        label.font = .caption
        return label
    }
}
