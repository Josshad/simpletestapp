//
//  NewsService.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation
import RxSwift

public protocol NewsService {
    func fetchNews() -> Single<NewsResponse>
    func fetchNewsContent(_ url: URL) -> Single<NewsDetails>
}

public struct NewsServiceImpl: NewsService {
    let httpClient: HTTPClientProtocol
    
    init(httpClient: HTTPClientProtocol) {
        self.httpClient = httpClient
    }
    
    public func fetchNews() -> Single<NewsResponse> {
        fetchNews(scheduler: ConcurrentDispatchQueueScheduler.httpClient)
    }
    
    public func fetchNewsContent(_ url: URL) -> Single<NewsDetails> {
        fetchNewsContent(url, scheduler: ConcurrentDispatchQueueScheduler.httpClient)
    }
    
    private func fetchNews(scheduler: ImmediateSchedulerType) -> Single<NewsResponse> {
        httpClient.run(request: NewsServiceFactory.fetchNews(), scheduler: scheduler)
    }
    
    private func fetchNewsContent(_ url: URL, scheduler: ImmediateSchedulerType) -> Single<NewsDetails> {
        httpClient.run(request: NewsServiceFactory.fetchNewsContent(url), scheduler: scheduler)
    }
}

enum NewsServiceFactory {
    private enum Const {
        static let newsURLString = "https://gist.githubusercontent.com/LorienMan/45b2f043d12afa7f80ed540737d49965/raw/df693198a1a1cf60b631f1efa11f84ac21257978/response.json"
    }
    static func fetchNews() -> Request<Void, NewsResponse> {
        guard let url = URL(string: Const.newsURLString) else {
            fatalError("Can't convert url from string")
        }
        return Request(url:url)
    }
    
    static func fetchNewsContent(_ url: URL) -> Request<Void, NewsDetails> {
        return Request(url:url)
    }
}
