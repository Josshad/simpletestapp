//
//  JSONDeseriazlizer.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

public protocol Deserializer {
    func model<T>(from data: Data) throws -> T where T: Decodable
}

struct JSONDeseriazlizer: Deserializer {
    static let `default` = JSONDeseriazlizer()
    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom(_dateParser(_:))
        return decoder
    }()

    func model<T>(from data: Data) throws -> T where T: Decodable {
        return try decoder.decode(T.self, from: data)
    }

    static func _dateParser(_ decoder: Decoder) throws -> Date {
        let stringDate = try decoder.singleValueContainer().decode(String.self)
        guard let date = DateFormatter.st_rfc3339.date(from: stringDate) else {
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: [
                NSLocalizedDescriptionKey: NSLocalizedString("Invalid server response JSON.", comment: "")
            ])
        }
        return date
    }
}
