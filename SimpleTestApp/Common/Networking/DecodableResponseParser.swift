//
//  DecodableResponseParser.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//


import Alamofire

struct DecodableResponseParser<SerializedObject: Decodable>: DataResponseSerializerProtocol {
    private let helper: ResponseParserHelper
    
    init(helper: ResponseParserHelper = GenericResponseParserHelper()) {
        self.helper = helper
    }
    
    func serialize(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) throws -> SerializedObject {
        if let error = error {
            throw error
        }
        
        return try helper.parse(response: response, data: data)
    }
}
