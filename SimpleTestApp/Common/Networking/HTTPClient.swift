//
//  HTTPClient.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

protocol NetworkSessionDataRequest {
    func response<ResponseType: Decodable>(parser: DecodableResponseParser<ResponseType>,
                                           scheduler: ImmediateSchedulerType) -> RxSwift.Single<ResponseType>
}

protocol NetworkSessionManager {
    func request(_ convertible: URLRequestConvertible) -> NetworkSessionDataRequest
}

extension Alamofire.Session: NetworkSessionManager {
    func request(_ convertible: URLRequestConvertible) -> NetworkSessionDataRequest {
        request(convertible, interceptor: nil)
    }
}

protocol HTTPClientProtocol {
    func run<U>(request: Request<Void, U>, scheduler: ImmediateSchedulerType) -> Single<U> where U: Decodable
}

class HTTPClient<Session: NetworkSessionManager>: NSObject, HTTPClientProtocol {
    
    private let sessionManager: Session
    
    typealias Parser<ResponseType> = (URLRequest?, HTTPURLResponse?, Data?, Error?) -> Result<ResponseType, Error>
    
    init(sessionManager: Session) {
        self.sessionManager = sessionManager
        super.init()
    }
    
    private func run<T>(
        _ request: RequestWrapper,
        parser: DecodableResponseParser<T>,
        scheduler: ImmediateSchedulerType
    ) -> Single<T> {
        Single.deferred { [weak self] in
            guard let self = self else { return .never() }
            return self.sessionManager
                .request(request)
                .response(parser: parser, scheduler: scheduler)
        }
    }
    
    func run<U>(request: Request<Void, U>, scheduler: ImmediateSchedulerType) -> Single<U>
        where U: Decodable {
            let wrapper = RequestWrapper.get(request.url)
            return run(wrapper, parser: DecodableResponseParser<U>(), scheduler: scheduler)
    }
    
    fileprivate enum RequestWrapper {
        case get(URL)
        
        var url: URL {
            switch self {
            case .get(let url): return url
            }
        }
        
        func body() throws -> Data? {
            switch self {
            case .get: return nil
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .get: return .get
            }
        }
    }
}

extension HTTPClient.RequestWrapper: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        var request = try URLRequest(url: url, method: method)
        request.httpBody = try body()
        request.headers.add(name: "Content-Type", value: "application/json")
        return request
    }
}

extension HTTPClient where Session == Alamofire.Session {
    static let shared: HTTPClient<Session> = HTTPClient(sessionManager: Alamofire.Session.default)
}
