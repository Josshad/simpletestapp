//
//  Request.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

struct Request<RequestType, ResponseType> {
    let url: URL
    let model: RequestType

    init(url: URL, model: RequestType, responseType: ResponseType.Type) {
        self.url = url
        self.model = model
    }

    init(url: URL, model: RequestType) {
        self.url = url
        self.model = model
    }
}

extension Request where RequestType == Void {
    init(url: URL, responseType: ResponseType.Type) {
        self.init(url: url, model: (), responseType: responseType)
    }

    init(url: URL) {
        self.init(url: url, model: ())
    }
}
