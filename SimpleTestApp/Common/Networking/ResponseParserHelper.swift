//
//  ResponseParserHelper.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation

protocol ResponseParserHelper {
    func check(response: HTTPURLResponse?, data: Data?) throws
    func parse<T>(response: HTTPURLResponse?, data: Data?) throws -> T where T: Decodable
}

struct GenericResponseParserHelper: ResponseParserHelper {
    private let deserializer: Deserializer

    init(deserializer: Deserializer = JSONDeseriazlizer.default) {
        self.deserializer = deserializer
    }

    func parse<T>(response: HTTPURLResponse?, data: Data?) throws -> T where T: Decodable {
        try check(response: response, data: data)

        guard let data = data, !data.isEmpty else {
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse)
        }

        do {
            return try deserializer.model(from: data)
        } catch {
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorCannotParseResponse)
        }
    }

    func check(response: HTTPURLResponse?, data: Data?) throws {
        guard let response = response else {
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse)
        }

        try checkStatusCode(response: response)
    }

    private func checkStatusCode(response: HTTPURLResponse) throws {
        guard response.statusCode == 100 || (200...205).contains(response.statusCode) else {
            let userInfo = [NSLocalizedDescriptionKey: "HTTP error \(response.statusCode)"]
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: userInfo)
            throw error
        }
    }
}
