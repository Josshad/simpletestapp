//
//  ImageDescriptor+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import UIKit

extension ImageDescriptor {
    public var url: URL {
        switch UIScreen.main.scale {
        case 2: return doubleResolutionURL
        case 3: return tripleResolutionURL
        default: return singleResolutionURL
        }
    }
}
