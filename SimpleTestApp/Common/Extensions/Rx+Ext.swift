//
//  Rx+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension SingleEvent {
    static func from<T>(result: Result<T, Error>) -> SingleEvent<T> {
        switch result {
        case .success(let value): return .success(value)
        case .failure(let error): return .error(error)
        }
    }
    
    static func from<T, E: Error>(result: Result<T, E>) -> SingleEvent<T> {
        switch result {
        case .success(let value): return .success(value)
        case .failure(let error): return .error(error)
        }
    }
}

extension ObservableType {
    func ignoreError() -> Observable<Element> {
        catchError { _ in .never() }
    }
}

extension PublishRelay where Element == Void {
    func accept() {
        accept(())
    }
}
