//
//  DispatchQueue+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Foundation
import RxSwift

private let kHTTPClientQueueLabel = "com.amaiz.http-client.completion"
private let kDownloadHTTPClientQueueLabel = "com.amaiz.http-download-client.completion"
private let kUploadHTTPClientQueueLabel = "com.amaiz.http-upload-client.completion"

extension DispatchQueue {
    private enum Const {
        static let httpClientQueueLabel = "com.simple-test.http-client"
    }
    static var httpClient = DispatchQueue(label: Const.httpClientQueueLabel, target: .global(qos: .utility))
}

extension ConcurrentDispatchQueueScheduler {
    static var httpClient = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.httpClient)
}
