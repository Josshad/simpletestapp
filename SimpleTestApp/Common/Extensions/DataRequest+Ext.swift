//
//  DataRequest+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import Alamofire
import RxSwift

extension DataRequest: NetworkSessionDataRequest {
    
    func response<ResponseType>(
        parser: DecodableResponseParser<ResponseType>,
        scheduler: ImmediateSchedulerType
    ) -> Single<ResponseType> {
        return Single.create { [weak self] block -> Disposable in
            guard let self = self else {
                return Disposables.create()
            }
            
            let composite = CompositeDisposable()
            
            let dataRequest = self.response(
                queue: .httpClient,
                responseSerializer: parser,
                completionHandler: { response in
                    _ = composite.insert(
                        scheduler.schedule(SingleEvent<ResponseType>.from(result: response.result)) {
                            block($0)
                            return Disposables.create()
                        }
                    )
            }
            )
            _ = composite.insert(Disposables.create(with: { dataRequest.cancel() }))
            self.resume()
            return composite
        }
    }
}
