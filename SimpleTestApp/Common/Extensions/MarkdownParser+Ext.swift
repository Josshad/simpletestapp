//
//  MarkdownParser+Ext.swift
//  SimpleTestApp
//
//  Created by Danila Gusev on 08.07.2020.
//  Copyright © 2020 Danila Gusev. All rights reserved.
//

import MarkdownKit

extension MarkdownParser {
    static let st_default: MarkdownParser = {
        let parser = MarkdownParser(font: .bodyItalic, color: .gray)
        return parser
    }()
}
